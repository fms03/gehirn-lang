# Gehirn Lang

Eine esoterische Programmiersprache wie [Brainfuck](https://de.wikipedia.org/wiki/Brainfuck), aber mit den längsten deutschen Wörtern laut [Duden](https://www.duden.de/sprachwissen/sprachratgeber/Die-langsten-Worter-im-Dudenkorpus).

Interpreter is a fork of [brainbash](https://github.com/Crestwave/brainbash).

## Commands

| GehirnLang | Brainfuck | Semantik |
|---|---|---|
| Rinderkennzeichnungsfleischetikettierungsüberwachungsaufgabenübertragungsgesetz | > | inkrementiert den Zeiger |
| Grundstücksverkehrsgenehmigungszuständigkeitsübertragungsverordnung | < | dekrementiert den Zeiger |
| Straßenentwässerungsinvestitionskostenschuldendienstumlage | + | inkrementiert den aktuellen Zellenwert |
| Unterhaltungselektroniktelefonverarbeitungspartner | - | dekrementiert den aktuellen Zellenwert |
| Arzneimittelversorgungswirtschaftlichkeitsgesetz | . | Gibt den aktuellen Zellenwert als ASCII-Zeichen auf der Standardausgabe aus |
| Erdachsendeckelscharnierschmiernippelkommission | , | Liest ein Zeichen von der Standardeingabe und speichert dessen ASCII-Wert in der aktuellen Zelle |
| Investitionsverwaltungsentwicklungsgesellschaft | [ | Springt nach vorne, hinter den passenden *Wochenstundenentlastungsbereinigungsverordnung*-Befehl, wenn der aktuelle Zellenwert 0 ist |
| Wochenstundenentlastungsbereinigungsverordnung | ] | Springt zurück, hinter den passenden *Investitionsverwaltungsentwicklungsgesellschaft*-Befehl, wenn der aktuelle Zellenwert nicht 0 ist |
